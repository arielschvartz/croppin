namespace :external_csv do
  desc "This task is called to import external CSV to create some promotions"

  task :import_all => :environment do
    Rake::Task["external_csv:import_aggregators"].invoke
    Rake::Task["external_csv:import_businesses"].invoke
    Rake::Task["external_csv:import_shoppings"].invoke
    Rake::Task["external_csv:import_branches"].invoke
  end

  task :import_aggregators, [:line] => [:environment] do |t, args|
    aggregator_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Agregadores.csv")
    aggregator_csv = CSV.parse(aggregator_csv_text, headers: true)

    puts "STARTING AGGREGATORS\n"
    aggregators = []
    aggregators_refs = []
    aggregator_csv.each_with_index do |row, i|
      if i < args[:line].to_i
        next
      end
      hash = row.to_hash

      unless hash['name'].blank?

        puts "Aggregator - #{hash['name']}"
        aggregators_refs.push hash.delete(nil)

        hash['logo'] = hash.delete('logo_image')

        unless hash['logo'].blank? or hash['logo'].include?('http') or hash['logo'].include?('www') or hash['logo'].include?('data:image')
          word = hash['logo']
          hash['logo'] = File.open("#{Rails.root}/external_promotion_import/agregadores/logos/#{word}.png", 'r')
          hash['logo_white'] = File.open("#{Rails.root}/external_promotion_import/agregadores/logos/#{word}-branco.png", 'r')
        end
        aggregator = Aggregator.new(hash)
        unless aggregator.save
          puts "#{aggregator.name} failed to save!\n"
          puts "Errors:\n"
          aggregator.errors.full_messages.each do |m|
            puts "#{m}\n"
          end
        end
        aggregators.push aggregator
      end
    end
  end

  task :import_businesses, [:line] => [:environment] do |t, args|
    business_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Estabelecimentos.csv")
    business_csv = CSV.parse(business_csv_text, headers: true)

    puts "\nSTARTING BUSINESSES\n"
    businesses = []
    businesses_refs = []
    business_csv.each_with_index do |row, i|
      if i < args[:line].to_i
        next
      end

      hash = row.to_hash

      unless hash['name'].blank?
        unless hash['logo'].blank? or hash['logo'].include?('http') or hash['logo'].include?('www') or hash['logo'].include?('data:image')
          hash['logo'] = File.open("#{Rails.root}/external_promotion_import/#{hash['logo']}", 'r')
        end

        puts "Business - #{hash['name']}"

        businesses_refs.push hash.delete('Ref. linha')

        phone = hash.delete('phone')

        business = Business.new(hash)

        unless phone.blank?
          phone.split(' ').each do |ph|
            new_phone = Phone.new
            new_phone.set_from_full_number(ph)
            business.phones.push new_phone
          end
        end

        unless business.save
          puts "\n#{business.name} failed to save!\n"
          puts "Errors:\n"
          business.errors.full_messages.each do |m|
            puts "#{m}\n"
          end
        end
        businesses.push business
      end
    end
  end

  task :import_shoppings, [:line] => [:environment] do |t, args|
    shoppings_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Shoppings.csv")
    shoppings_csv = CSV.parse(shoppings_csv_text, headers: true)

    puts "\nSTARTING SHOPPINGS\n"
    shoppings = []
    shoppings_csv.each_with_index do |row, i|
      if i < args[:line].to_i
        next
      end
      hash = row.to_hash
      if hash['name'].blank?
        break
      end

      puts "Shopping - #{hash['name']}"

      zip_code = hash.delete('zip_code')
      number = hash.delete('number')
      business = hash.delete('administrator_business')

      phone = hash.delete('phone')

      unless hash['logo'].blank? or hash['logo'].include?('http') or hash['logo'].include?('www') or hash['logo'].include?('data:image')
        hash['logo'] = File.open("#{Rails.root}/external_promotion_import/#{hash['logo']}", 'r')
      end

      shopping = Shopping.new(hash)

      unless zip_code.blank?
        shopping.address = Address.new
        shopping.address.zip_code = zip_code
        shopping.address.number = number
      end

      unless phone.blank?
        phone.split(' ').each do |ph|
          new_phone = Phone.new
          new_phone.set_from_full_number(ph)
          shopping.phones.push new_phone
        end
      end

      unless shopping.save
        puts "\n#{shopping.name} failed to save!\n"
        puts "Errors:\n"
        shopping.errors.full_messages.each do |m|
          puts "#{m}\n"
        end
      end
      shoppings.push shopping
    end
  end

  task :import_branches, [:line] => [:environment] do |t, args|
    branches_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Filiais.csv")
    branches_csv = CSV.parse(branches_csv_text, headers: true)

    puts "\nSTARTING BRANCHES\n"
    branches = []
    branches_csv.each_with_index do |row, i|
      if i < args[:line].to_i
        next
      end
      hash = row.to_hash
      if hash['business'].blank?
        break
      end

      business_name = hash.delete('business')
      business_id = hash.delete('Ref. Estabelecimento')

      business = Business.find(business_id)

      latlong = hash.delete('LatLong')
      zip_code = hash.delete('zip_code')
      number = hash.delete('number')
      complement = hash.delete('complement')

      phone = hash.delete('phone')

      shopping = Shopping.find_by_name(hash.delete('shopping'))

      unless business.nil? and shopping.nil?
        puts "#{business.name}\n"
        branch = Branch.new(hash)
        branch.business = business
        branch.shopping = shopping

        if latlong.present?
          latlong = latlong[1..-1]
          branch.address = Address.new
          branch.address.latitude = latlong.split(',').first.to_f
          branch.address.longitude = latlong.split(',').last.to_f
          if number
            branch.address.number = number
          end
          if complement
            branch.address.complement = complement
          end
          if zip_code
            branch.address.zip_code ||= zip_code
          end
        elsif zip_code.present?
          branch.address = Address.new
          branch.address.zip_code = zip_code
          branch.address.number = number
          branch.address.complement = complement
        end

        if branch.address
          branch.address.valid?
        end

        unless phone.blank?
          phone.split(' ').each do |ph|
            new_phone = Phone.new
            new_phone.set_from_full_number(ph)
            branch.phones.push new_phone
          end
          phone.valid?
        end

        unless branch.save
          puts "\n#{branch.name} failed to save!\n"
          puts "Errors:\n"
          branch.errors.full_messages.each do |m|
            puts "#{m}\n"
          end
        end

        branches.push branch
      end
    end
  end




  task :import_promotions, [:line] => [:environment] do |t, args|
    promotions_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Promoções.csv")
    promotions_csv = CSV.parse(promotions_csv_text, headers: true)

    branches_csv_text = File.read("#{Rails.root}/external_promotion_import/Promoções - Primeira Carga - Filiais.csv")
    branches_csv = CSV.parse(branches_csv_text, headers: true)

    puts "\nSTARTING PROMOTIONS\n"
    promotions = []
    promotions_csv.each_with_index do |row, i|
      if i < args[:line].to_i
        next
      end
      hash = row.to_hash
      if hash['aggregator'].blank?
        break
      end
      hash['points_needed'] = hash['points_needed'].to_i
      hash['difficulty'] = hash['difficulty'].to_i

      aggregator_name = hash.delete('aggregator')
      aggregator_id = hash.delete('Ref. Agregador')
      aggregator = Aggregator.find_by_name(aggregator_name)

      branches_ids = hash.delete('filial_ids').split(',').map { |x| x.to_i - 1 }.sort_by { |x| x }
      branches = Branch.find(branches_ids)


      images = hash.delete('images')
      unless images.nil?
        images = images.split(' ')
      end

      week_days = hash.delete('week_days')

      old_date = hash.delete('expiration_date')
      unless old_date.nil?
        old_date = old_date.split('/')
        if old_date.length == 3
          hash['expiration_date'] = Time.new(old_date.last, old_date[1], old_date.first)
        end
      end

      category_name = hash.delete('sub_category')

      puts "\nPromotion #{i} - #{hash['title']}\n"

      unless aggregator.blank? and branches.empty?
        promotion = Promotion.new(hash)
        promotion.aggregator = aggregator
        promotion.branches = branches

        unless category_name.nil?
          category = Category.find_by_name category_name
          if category.nil?
            category = Category.create name: category_name
          end
          promotion.category = category
        end

        unless images.nil?
          images.each do |img|
            puts "loading image - #{img}\n"
            new_img = img
            unless img.blank? or img.include?('http') or img.include?('www') or img.include?('data:image')
              new_img = File.open("#{Rails.root}/external_promotion_import/#{img}", 'r')
            end
            prom_image = promotion.promotion_images.new
            prom_image.image = new_img
          end
        end

        open_day = OpenDay.new
        if week_days == 'online'
          open_day.set_days "1111111"
        else
          days = week_days.chars.map { |x| x.to_i }
          final_string = ""
          (0..6).each do |i|
            if days.include? i
              final_string += "1"
            else
              final_string += "0"
            end
          end
          open_day.set_days final_string
        end
        promotion.open_day = open_day
        open_day.promotion = promotion

        promotions.push promotion
        promotion.save
      end
    end
  end
end
