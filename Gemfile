source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.4'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

group :development do
  gem 'letter_opener'
  gem 'better_errors'
  gem 'nifty-generators'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'pry-rails'
  gem 'pry'
  gem 'dotenv-rails'
end

group :test do
  gem 'rspec-its'
  gem 'capybara'
  gem 'shoulda-matchers'
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'webmock'
  gem 'vcr'
  gem 'mocha', :require => false
end

# Heroku related
group :production do
  gem 'rails_12factor'
  gem 'heroku-deflater'
  gem 'newrelic_rpm'
end

gem 'new_relic_ping'

# Front-end Related Gems
gem 'haml-rails'
gem 'bootstrap-sass'
# gem 'font-awesome-rails'
gem 'bourbon'
gem 'formtastic', '~> 2.3.0.rc4'
gem 'maskedinput-rails'
# gem 'underscore-rails'
# gem 'gmaps4rails'

# Image upload related
gem 'paperclip'
gem 'aws-sdk'

# Model related
gem 'friendly_id'
gem 'paranoia'
gem 'mail_form'
gem 'awesome_nested_set'
gem 'cpf_cnpj'
gem 'br_zip_code'
gem 'countries'
gem 'phony_rails'
gem 'geocoder'
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'pg_search'

# Controller related
gem 'inherited_resources'
gem 'silencer', '~> 1.0.0.rc2'

# Admin related
gem 'activeadmin', github: 'activeadmin/active_admin'
gem 'tinymce_aws_file_upload', '~> 0.1.0'
gem 'country-select'
gem 'activeadmin_expandable_inputs', '~> 0.0.4'
gem 'activeadmin_file_input_on_steroids', '~> 0.0.2'

# Other
gem 'spin_loader', '~> 0.1.0'

ruby '2.0.0'
