require 'rails_helper'

RSpec.describe Category, type: :model do
  it_should_behave_like "model_with_slug", :name

  it { should have_many :promotions }
  it { should have_many :highlights }

  it { should validate_presence_of :name }

  it { should validate_uniqueness_of :name }

  it { should have_many :promotion_views }
  it { should have_many :promotion_clicks }
  it { should have_many :promotion_likes }
  it { should have_many :promotion_reports }

  [:name, :slug, :promotions, :highlights, :promotion_views, :promotion_clicks, :promotion_likes, :promotion_reports].each do |attr|
    it { should respond_to attr }
  end

  context "highlights" do
    let(:category) { create(:category) }

    subject { category }

    describe "should only include the highlighted promos" do
      before do
        @promo1 = build(:promotion)
        @promo1.highlight = true
        @promo1.category = category
        @promo1.save

        @promo2 = build(:promotion)
        @promo2.category = category
        @promo2.save
      end

      its(:highlights) { should == [@promo1] }
    end
  end
end
