require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many :promotion_views }
  it { should have_many :promotion_clicks }
  it { should have_many :promotion_likes }
  it { should have_many :promotion_reports }

  it { should have_and_belong_to_many :aggregators }
end
