require 'rails_helper'

RSpec.describe Address, type: :model do

  [:country, :latitude, :longitude].each do |attr|
    it { should validate_presence_of attr }
  end

  [:owner, :zip_code, :street, :number, :complement, :neighborhood, :city, :state, :country].each do |attr|
    it { should respond_to attr }
  end

  it { should belong_to :owner }

  it_behaves_like "model_with_zip_code"

  describe "country" do
    let(:address) { build(:address) }

    before do
      address.country = nil
      address.save
    end
    it { address.country.downcase.should == "br" }
  end

  context "geo info should be auto filled when latitude and longitude are present", :vcr do
    let(:address) { build(:address) }

    before do
      address.country = nil
      address.state = nil
      address.city = nil
      address.neighborhood = nil
      address.street = nil
      address.number = nil
      address.zip_code = nil
      address.save
      address.reload
    end

    subject { address }

    its(:country) { should == "BR" }
    its(:state) { should == "RJ" }
    its(:city) { should == "Rio" }
    its(:neighborhood) { should ==  "Leblon" }
    its(:street) { should ==  "Rua General Venâncio Flôres" }
    its(:number) { should ==  "113" }
    its(:zip_code) { should == "22441090" }
  end

  context "latitude and longitude should be filled when the rest is present", :vcr do
    let(:address) { build(:address) }

    before do
      address.latitude = nil
      address.longitude = nil
      address.save
      address.reload
    end

    subject { address }

    its(:latitude) { should == -22.9862686 }
    its(:longitude) { should == -43.224573 }
  end
end
