require 'rails_helper'

RSpec.describe Aggregator, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :website }

  it { should validate_uniqueness_of :name }

  it { should have_attached_file(:logo) }
  it { should validate_attachment_presence(:logo) }
  it { should validate_attachment_content_type(:logo).allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }

  it { should have_attached_file(:logo_white) }
  it { should validate_attachment_presence(:logo_white) }
  it { should validate_attachment_content_type(:logo_white).allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }

  [:name, :slug, :website, :participants_details, :default_difficulty, :logo, :logo_white, :promotions, :users].each do |attr|
    it { should respond_to attr }
  end

  it { should have_many :promotions }
  it { should have_and_belong_to_many :users }

  it_should_behave_like "model_with_slug", :name

  describe "website validation" do

    valid_urls = ["http://www.google.com", "https://www.google.com", "http://google.com", "https://google.com", "www.google.com", "google.com"]
    valid_urls.each do |url|
      it { should allow_value(url).for(:website) }
    end

    invalid_urls = ["htt://google.com", "google.!com"]
    invalid_urls.each do |url|
      it { should_not allow_value(url).for(:website) }
    end
  end
end
