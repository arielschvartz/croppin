require 'rails_helper'

RSpec.describe OpenDay, type: :model do
  let(:open_day) { build(:open_day) }

  subject { open_day }

  [:promotion, :sun, :mon, :tue, :wed, :thu, :fri, :sat].each do |attr|
    it { should respond_to attr }
  end

  it { should validate_presence_of :promotion }

  context "binary set" do
    describe "with correct argument" do
      before { open_day.set_days "0110110" }
      its(:sun) { should eq(false) }
      its(:mon) { should eq(true) }
      its(:tue) { should eq(true) }
      its(:wed) { should eq(false) }
      its(:thu) { should eq(true) }
      its(:fri) { should eq(true) }
      its(:sat) { should eq(false) }
    end

    it "with wrong number of binaries" do
      expect do
        open_day.set_days "0011"
      end.to raise_error ArgumentError
    end

    it "with wrong type of argument" do
      expect do
        open_day.set_days 1100110
      end.to raise_error ArgumentError
    end
  end

  describe "when nothing set, all should be false" do
    before { open_day.save }
    its(:sun) { should eq(false) }
    its(:mon) { should eq(false) }
    its(:tue) { should eq(false) }
    its(:wed) { should eq(false) }
    its(:thu) { should eq(false) }
    its(:fri) { should eq(false) }
    its(:sat) { should eq(false) }
  end

  describe "humanize" do
    it { should respond_to :humanize }
    describe "nenhum dia aberto" do
      before { open_day.set_days "0000000" }
      its(:humanize) { should == "fechado" }
    end
    describe "aberto dias de semana" do
      before { open_day.set_days "0111110" }
      its(:humanize) { should == "somente dias de semana" }
    end
    describe "aberto finais de semana" do
      before { open_day.set_days "1000001" }
      its(:humanize) { should == "somente finais de semana" }
    end
    describe "dias aleatórios" do
      before { open_day.set_days "1111111" }
      its(:humanize) { should == "domingo, segunda, terça, quarta, quinta, sexta, sábado" }
    end
  end
end
