require 'rails_helper'

RSpec.describe Phone, type: :model do
  [:number].each do |attr|
    it { should validate_presence_of attr }
  end

  [:owner, :country_code, :city_code, :number, :extension_line].each do |attr|
    it { should respond_to attr }
  end

  it { should belong_to :owner }

  describe "when no country code" do
    let(:phone) { build(:phone) }

    before do
      phone.country_code = nil
      phone.save
    end
    it { phone.country_code.should == 55 }
  end

  describe "formatted method" do

    it { should respond_to :formatted }

    let(:phone) { create(:brazilian_phone) }

    subject { phone }

    it { phone.formatted(:br, false).should match(/\A[0]\d{2}[ ]\d{4,5}[ ]\d{4,5}\z/) }

    it { phone.formatted.should match(/\A[+]\d{2}[ ]\d{2}[ ]\d{4,5}[ ]\d{4,5}\z/) }
  end
end
