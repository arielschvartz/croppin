require 'rails_helper'

RSpec.describe PromotionImage, type: :model do
  it { should validate_presence_of :image }
  it { should validate_presence_of :promotion }

  [:image, :promotion, :position].each do |attr|
    it { should respond_to attr }
  end

  it { should have_attached_file(:image) }
  it { should validate_attachment_presence(:image) }
  it { should validate_attachment_content_type(:image).allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }

  it { should belong_to :promotion }

  context "scope order" do
    let(:promotion_image) { create(:promotion_image) }

    subject { promotion_image }

    describe "should be default to position" do
      before do
        new_promo = promotion_image.dup
        new_promo.position = new_promo.position - 10
        new_promo.save

        other_new_promo = promotion_image.dup
        other_new_promo.position = other_new_promo.position + 10
        other_new_promo.save

        promotion_image.reload
        promotion_image.promotion.reload
      end
      it { promotion_image.promotion.promotion_images.should == PromotionImage.where(promotion_id: promotion_image.promotion.id).order(:position) }
    end
  end
end
