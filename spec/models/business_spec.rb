require 'rails_helper'

RSpec.describe Business, type: :model do

  # it { should validate_presence_of :corporate_name }
  it { should validate_presence_of :name }
  it { should validate_presence_of :logo }
  # it { should validate_presence_of :cnpj }
  it { should validate_presence_of :about }
  # it { should validate_presence_of :phone }

  it { should validate_uniqueness_of :cnpj }
  it { should validate_uniqueness_of :name }

  it { should have_attached_file(:logo) }
  it { should validate_attachment_presence(:logo) }
  it { should validate_attachment_content_type(:logo).allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }

  [:corporate_name, :name, :logo, :cnpj, :about, :email].each do |attr|
    it { should respond_to attr }
  end

  it { should have_many :branches }
  it { should have_one :address }
  it { should have_many :phones }

  it_behaves_like "model_with_cnpj"
  it_behaves_like "model_with_email"
  it_behaves_like "model_with_slug", :name
end
