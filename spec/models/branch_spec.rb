require 'rails_helper'

RSpec.describe Branch, type: :model do
  let(:branch) { build(:branch) }

  subject { branch }

  it { should have_one :address }
  it { should have_many :phones }
  it { should belong_to :business }
  it { should belong_to :shopping }
  it { should have_and_belong_to_many :promotions }

  it { should validate_presence_of :business }

  describe "when has no address and no website" do
    before do
      branch.address = nil
      branch.website = nil
    end
    it { should_not be_valid }
  end

  describe "when has no address" do
    before { branch.address = nil }
    it { should be_valid }
  end

  describe "when has no website" do
    before { branch.website = nil }
    it { should be_valid }
  end

  describe "when has address and website" do
    it { should be_valid }
  end

  describe "website validation" do

    valid_urls = ["http://www.google.com", "https://www.google.com", "http://google.com", "https://google.com", "www.google.com", "google.com"]
    valid_urls.each do |url|
      it { should allow_value(url).for(:website) }
    end

    invalid_urls = ["htt://google.com", "google.!com"]
    invalid_urls.each do |url|
      it { should_not allow_value(url).for(:website) }
    end
  end

  describe "associations changes" do
    before { branch.save }

    it "does update slug when business name is changed" do
      expect do
        branch.business.reload
        branch.business.name = "teste"
        branch.business.save
        branch.reload
      end.to change{ branch.slug }
    end

    it "does not change slug when business name is not changed" do
      expect do
        branch.business.reload
        branch.business.corporate_name = "teste"
        branch.business.save
        branch.reload
      end.not_to change{ branch.slug }
    end

    it "does update slug when address neighborhood is changed" do
      expect do
        branch.address.reload
        branch.address.neighborhood = "teste"
        branch.address.save
        branch.reload
      end.to change{ branch.slug }
    end

    it "does not change slug when address neighborhood is not changed" do
      expect do
        branch.address.reload
        branch.address.city = "teste"
        branch.address.save
        branch.reload
      end.not_to change{ branch.slug }
    end

    it "does update slug when address is destroyed" do
      expect do
        branch.address.reload
        branch.address.destroy
        branch.reload
      end.to change{ branch.slug }
    end
  end

  it_behaves_like "model_with_slug"
end
