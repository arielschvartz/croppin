require 'rails_helper'

RSpec.describe Shopping, type: :model do

  it { should have_one :address }
  it { should have_many :phones }
  it { should have_many :branches }
  it { should belong_to :administration_business }

  it { should validate_presence_of :address }
  # it { should validate_presence_of :administration_business }

  it { should validate_presence_of :name }
  it { should validate_uniqueness_of :name }

  it { should have_attached_file(:logo) }
  it { should validate_attachment_presence(:logo) }
  it { should validate_attachment_content_type(:logo).allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }

  [:name, :website, :logo].each do |attr|
    it { should respond_to attr }
  end

  it_behaves_like "model_with_slug", :name
end
