require 'rails_helper'

RSpec.describe Promotion, type: :model do

  [:title, :description, :restriction, :terms_of_use, :slug, :website, :points_needed, :difficulty, :expiration_date, :number_of_reports, :number_of_likes, :number_of_clicks, :number_of_views, :aggregator, :category, :branches, :promotion_images, :open_day, :promotion_views, :promotion_clicks, :promotion_likes, :promotion_reports].each do |attr|
    it { should respond_to attr }
  end

  it { should belong_to :aggregator }
  it { should belong_to :category }
  it { should have_and_belong_to_many :branches }
  it { should have_many :promotion_images }
  it { should have_one :open_day }

  it { should have_many :promotion_views }
  it { should have_many :promotion_clicks }
  it { should have_many :promotion_likes }
  it { should have_many :promotion_reports }

  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :website }
  it { should validate_numericality_of :points_needed }
  it { should validate_numericality_of :difficulty }

  [-1, -10, -1000, 1.1, 5.5, 0.5].each do |n|
    it { should_not allow_value(n).for(:points_needed) }
    it { should_not allow_value(n).for(:difficulty) }
  end

  [0, 5, 10, 1000].each do |n|
    it { should allow_value(n).for(:points_needed) }
  end

  [0, 1, 2, 3, 4, 5].each do |n|
    it { should allow_value(n).for(:difficulty) }
  end

  it_behaves_like "model_with_slug", :title

  describe "views, clicks, likes and reports should start as 0" do
    let(:promotion) { create(:promotion) }
    subject { promotion }

    its(:number_of_views) { should eq(0) }
    its(:number_of_clicks) { should eq(0) }
    its(:number_of_likes) { should eq(0) }
    its(:number_of_reports) { should eq(0) }
  end

  describe "website validation" do

    valid_urls = ["http://www.google.com", "https://www.google.com", "http://google.com", "https://google.com", "www.google.com", "google.com"]
    valid_urls.each do |url|
      it { should allow_value(url).for(:website) }
    end

    invalid_urls = ["htt://google.com", "google.!com"]
    invalid_urls.each do |url|
      it { should_not allow_value(url).for(:website) }
    end
  end
end
