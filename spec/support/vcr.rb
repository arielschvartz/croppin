require 'vcr'

VCR.configure do |c|
  # c.debug_logger = File.open("#{Rails.root}/spec/vcr.log", 'w')
  # c.allow_http_connections_when_no_cassette = true

  #the directory where your cassettes will be saved
  c.cassette_library_dir = 'spec/cassettes'

  # your HTTP request service. You can also use fakeweb, webmock, and more
  c.hook_into :webmock
  c.configure_rspec_metadata!
  c.default_cassette_options = { :record => :new_episodes }

end
