RSpec.shared_examples_for "model_with_zip_code" do

  let(:model) { build(described_class.to_s.downcase.to_sym) }

  valid_zip_codes = ["22.441-090", "22441090", "22451-060", "22.451060"]
  valid_zip_codes.each do |zip_code|
    it { should allow_value(zip_code).for(:zip_code) }
  end

  invalid_zip_codes = ["1234567", "123456789", "12.344.444", "22-441-090", "22.441--090"]
  invalid_zip_codes.each do |zip_code|
    it { should_not allow_value(zip_code).for(:zip_code) }
  end

  describe "after save" do
    before do
      model.zip_code = "22.441-090"
      model.save
    end
    it "should be stripped" do
      model.zip_code.should == "22441090"
    end
    it "should have a formatted method" do
      model.formatted_zip_code.should == "22.441-090"
    end
  end

  context "autocomplete by zip code", :vcr do
    subject { model }

    address_attributes = [:street, :neighborhood, :city, :state]
    address_values = ["rua general venâncio flores", "leblon", "rio de janeiro", "rj"]
    class_attributes = described_class.new.attributes.keys.map { |x| x.to_sym }


    address_attributes.each_with_index do |k, i|
      if class_attributes.include? k
        describe "when #{k.to_s} is empty" do
          before do
            model[k] = nil
            model.save
          end
          it { model[k].downcase.should == address_values[i] }
        end
      end
    end
  end
end
