RSpec.shared_examples_for "model_with_email" do

  let(:model) { build(described_class.to_s.downcase.to_sym) }

  valid_emails = %w[user@foo.com A_Us-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
    valid_emails.each do |email|
      it { should allow_value(email).for(:email) }
    end

    invalid_emails = %w[user@foo,com user_at_foo.org example.user@foo. foo@bar_baz.com foo@bar+baz.com]
    invalid_emails.each do |email|
      it { should_not allow_value(email).for(:email) }
    end

    describe "should be downcased" do
      before do
        model.email = model.email.random_case
        model.save
      end
      it do
        model.email.should == model.email.downcase
      end
    end

end
