RSpec.shared_examples_for "model_as_promotion_statistics" do

  let(:model) do
    described_class.new promotion: create(:promotion), user: create(:user), ip: '201.29.226.130', latitude: -22.9, longitude: -43.2333, country: 'Brazil', state: 'Rio de Janeiro', city: 'Rio de Janeiro'
  end

  subject { model }

  it { should belong_to :promotion }
  it { should belong_to :user }

  [:ip, :latitude, :longitude, :country, :state, :city, :promotion, :user].each do |attr|
    it { should respond_to attr }
  end

  it { should validate_presence_of :promotion }
  # it { should validate_presence_of :user }
  it { should validate_presence_of :ip }

  valid_ips = ["192.168.0.1", "201.200.180.171", "10.0.0.1"]
  valid_ips.each do |ip|
    it { should allow_value(ip).for(:ip) }
  end

  valid_ips = ["192.168.0", "201.200", "10", "999.100.100.100", "100.999.100.100", "100.100.999.100", "100.100.100.999"]
  valid_ips.each do |ip|
    it { should_not allow_value(ip).for(:ip) }
  end

  context "after save" do
    it "should increment promotion number_of_#{described_class.to_s[9..-1].downcase}s" do
      expect do
        model.save
      end.to change { model.promotion["number_of_#{described_class.to_s[9..-1].downcase}s".to_sym] }
    end

    it "should increment category number_of_#{described_class.to_s[9..-1].downcase}s" do
      expect do
        model.save
      end.to change { model.promotion.category["number_of_#{described_class.to_s[9..-1].downcase}s".to_sym] }
    end
  end

  context "geo info should be auto filled when blank", :vcr do
    before do
      model.latitude = nil
      model.longitude = nil
      model.country = nil
      model.state = nil
      model.city = nil
      model.save
      model.reload
    end
    its(:latitude) { should == -22.9 }
    its(:longitude) { should == -43.2333 }
    its(:country) { should == "Brazil" }
    its(:state) { should == "Rio de Janeiro" }
    its(:city) { should == "Rio De Janeiro" }
  end

end
