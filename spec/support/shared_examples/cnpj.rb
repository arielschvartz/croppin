RSpec.shared_examples_for "model_with_cnpj" do

  let(:model) { build(described_class.to_s.downcase.to_sym) }

  valid_cnpjs = ["69.103.604/0001-60", "69103604000160", CNPJ.generate, CNPJ.generate(true)]
  valid_cnpjs.each do |cnpj|
    it { should allow_value(cnpj).for(:cnpj) }
  end

  invalid_cnpjs = ["69.103.604/0001-61", "69.103.604/0001-59", "0000", "0", "asjfasfkjahslkf"]
  invalid_cnpjs.each do |cnpj|
    it { should_not allow_value(cnpj).for(:cnpj) }
  end

  describe "after save" do
    before do
      model.cnpj = CNPJ.generate(true)
      model.save
    end
    it "should be stripped" do
      model.cnpj.should == CNPJ.new(model.cnpj).stripped
    end
    it "should have a formatted method" do
      model.formatted_cnpj.should == CNPJ.new(model.cnpj).formatted
    end
  end

end
