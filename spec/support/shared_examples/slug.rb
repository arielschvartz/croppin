RSpec.shared_examples_for "model_with_slug" do |slug_column|

  let(:model) { build(described_class.to_s.downcase.to_sym) }

  describe "expects slug to be generated" do
    before { model.save }
    it { model.slug.should_not be_blank }
  end

  if slug_column.present? and described_class.new.attributes.keys.map { |x| x.to_sym }.include? slug_column
    describe "expects slug to change if #{slug_column.to_s} changes" do
      before do
        model.save
        model[slug_column] = "nome teste"
        model.save
        model.reload
      end
      it { model.slug.should == "nome-teste" }
    end
  end

end
