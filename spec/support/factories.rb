require 'ffaker'
require 'securerandom'

FactoryGirl.define do
  sequence(:email) { |n| "#{n}#{Faker::Internet.email}" }
  # sequence(:phone_number) { |n| n.to_s.rjust(10, "0") }
  # sequence(:cellphone_number) { |n| n.to_s.rjust(11, "0") }
  sequence(:lorem) { Faker::Lorem.paragraphs.join }
  sequence(:difficulty) { |n| n%6 }
  sequence(:business_name) { Faker::Company.name }
  sequence(:phone_country_code) { rand(10..99) }
  sequence(:phone_city_code) { rand(10..99) }
  sequence(:phone_number) { rand(100000000..999999999).to_s.rjust(9, "0") }
  sequence(:phone_extension_line) { rand(0..99) }
  sequence(:title) { Faker::Lorem.sentence }
  sequence(:website) { Faker::Internet.http_url }
  sequence(:points_needed) { rand(0..1000).to_i }
  sequence(:expiration_date) { Date.today + rand(0..365).days }
  sequence(:category_name) { Faker::BaconIpsum.word }

  factory :address do
    zip_code "22441090"
    street "Rua General Venâncio Flores"
    number "105"
    complement "301"
    neighborhood "Leblon"
    city "Rio de Janeiro"
    state "RJ"
    latitude -22.9862686
    longitude -43.224573
  end

  factory :phone do
    country_code { 55 }
    city_code { 21 }
    number { 996318859 }
    extension_line { generate(:phone_extension_line) }

    factory :brazilian_phone do
      country_code 55
    end
  end

  factory :business do
    corporate_name { generate(:business_name) }
    name { generate(:business_name) }
    association :address, factory: :address
    logo_file_name { 'rails.png' }
    logo_content_type { 'image/png' }
    logo_file_size { 256 }
    cnpj { CNPJ.generate(true) }
    about { generate(:lorem) }
    email { generate(:email) }
    default_difficulty { generate(:difficulty) }
    after(:build) do |model|
      model.phones.push FactoryGirl.create(:phone)
    end
    # association :phone, factory: :phone

    after(:build) do |asset|
      image_file = Rails.root.join("app/assets/images/#{asset.logo_file_name}")

      # cp test image to direcotries
      [:original, :medium, :thumb].each do |size|
        dest_path = asset.logo.path(size)
        `mkdir -p #{File.dirname(dest_path)}`
        `cp #{image_file} #{dest_path}`
      end
    end
  end

  factory :shopping do
    name { generate(:business_name) }
    website { generate(:website) }
    association :administration_business, factory: :business
    association :address, factory: :address
    after(:build) do |model|
      model.phones.push FactoryGirl.create(:phone)
    end
    # association :phone, factory: :phone

    logo_file_name { 'rails.png' }
    logo_content_type { 'image/png' }
    logo_file_size { 256 }

    after(:build) do |asset|
      image_file = Rails.root.join("app/assets/images/#{asset.logo_file_name}")

      # cp test image to direcotries
      [:original, :medium, :thumb].each do |size|
        dest_path = asset.logo.path(size)
        `mkdir -p #{File.dirname(dest_path)}`
        `cp #{image_file} #{dest_path}`
      end
    end
  end

  factory :branch do
    association :business, factory: :business
    association :address, factory: :address
    after(:build) do |model|
      model.phones.push FactoryGirl.create(:phone)
    end
    # association :phone, factory: :phone
    website { generate(:website) }
  end

  factory :open_day do
  end

  factory :category do
    name { generate(:category_name) }
  end

  factory :promotion do
    title { generate(:title) }
    description { generate(:lorem) }
    restriction { generate(:lorem) }
    terms_of_use { generate(:lorem) }
    website { generate(:website) }
    points_needed { generate(:points_needed) }
    difficulty { generate(:difficulty) }
    association :category, factory: :category
    after(:create) do |prom|
      prom.branches = [FactoryGirl.create(:branch)]
      prom.open_day = FactoryGirl.create(:open_day, promotion_id: prom.id)
    end
  end

  factory :promotion_image do
    association :promotion, factory: :promotion
    position { rand(0..1000) }

    image_file_name { 'rails.png' }
    image_content_type { 'image/png' }
    image_file_size { 256 }

    after(:build) do |asset|
      image_file = Rails.root.join("app/assets/images/#{asset.image_file_name}")

      # cp test image to direcotries
      [:original, :medium, :thumb].each do |size|
        dest_path = asset.image.path(size)
        `mkdir -p #{File.dirname(dest_path)}`
        `cp #{image_file} #{dest_path}`
      end
    end
  end

  factory :aggregator do
    name { generate(:business_name) }
    participants_details { generate(:lorem) }
    default_difficulty { rand(0..10) }
    website { generate(:website) }

    logo_file_name { 'rails.png' }
    logo_content_type { 'image/png' }
    logo_file_size { 256 }

    logo_white_file_name { 'rails.png' }
    logo_white_content_type { 'image/png' }
    logo_white_file_size { 256 }

    after(:build) do |asset|
      image_file = Rails.root.join("app/assets/images/#{asset.logo_file_name}")

      # cp test image to direcotries
      [:original, :medium, :thumb].each do |size|
        dest_path = asset.logo.path(size)
        `mkdir -p #{File.dirname(dest_path)}`
        `cp #{image_file} #{dest_path}`
        dest_path1 = asset.logo_white.path(size)
        `mkdir -p #{File.dirname(dest_path1)}`
        `cp #{image_file} #{dest_path1}`
      end
    end
  end

  factory :user do
    email { generate(:email) }
    password { SecureRandom.hex }
  end
end
