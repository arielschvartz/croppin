require 'rails_helper'

RSpec.describe PromotionsController, type: :controller do

  describe "routing" do
    it "routes /promocoes" do
      { get: '/promocoes' }.should route_to(
        controller: 'promotions',
        action: 'index'
      )
    end

    it "routes /promocoes/:id" do
      promotion = create(:promotion)
      { get: "/promocoes/#{promotion.id}" }.should route_to(
        controller: 'promotions',
        action: 'show',
        id: promotion.id.to_s
      )
    end
  end

  describe "GET 'index'" do
    before do
      @promotion = create(:promotion)
      get :index
    end

    it "returns http success" do
      response.should be_success
    end

    it "assigns @promotions" do
      expect(assigns(:promotions)).to eq([@promotion])
    end

    it "renders index template" do
      expect(response).to render_template('index')
    end
  end

  describe "GET 'show'" do
    before do
      @promotion = create(:promotion)
      get :show, { id: @promotion.id }
    end

    it "returns http success" do
      expect(response).to be_success
    end

    it "assigns @promotion" do
      expect(assigns(:promotion)).to eq(@promotion)
    end

    it "render show page" do
      expect(response).to render_template('show')
    end
  end

end
