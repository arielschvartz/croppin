require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do

  describe "routing" do
    it "routes /categorias/:id" do
      category = create(:category)
      { get: "/categorias/#{category.id}" }.should route_to(
        controller: 'categories',
        action: 'show',
        id: category.id.to_s
      )
    end
  end

  describe "GET 'show'" do
    before do
      @promotion = create(:promotion)
      @category = @promotion.category
      get :show, { id: @category.id }
    end

    it "returns http success" do
      response.should be_success
    end

    it "assigns @category" do
      expect(assigns(:category)).to eq(@category)
    end

    it "assigns @promotions" do
      expect(assigns(:promotions)).to eq([@promotion])
    end

    it "renders show page" do
      expect(response).to render_template('show')
    end
  end

end
