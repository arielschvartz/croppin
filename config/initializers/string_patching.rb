class String
  def random_case
    a = ""
    self.chars.each do |c|
      if rand(2).floor > 0
        a += c.upcase
      else
        a += c.downcase
      end
    end
    a
  end
end