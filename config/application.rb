require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Croppin
  class Application < Rails::Application
    # Config Rails Generators
    config.generators do |g|
      g.helper false
      g.assets false
      g.integration_tool :rspec
      g.view_specs false
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Brasilia'

    # Set available locales
    config.i18n.enforce_available_locales = true

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.available_locales = [:'pt-BR']
    config.i18n.default_locale = :'pt-BR'


    # Prevent initializing your application and connect to the database on assets precompile.
    # config.assets.initialize_on_precompile = false

    # Autoload Lib
    config.autoload_paths += %W(#{config.root}/lib)

    config.assets.paths << Rails.root.join('app', 'assets', 'svgs')
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.precompile += %w( .svg .eot .ttf .woff )

  end
end


# Load custom initializers

require File.expand_path('../pre-env-initializers/load_aws_config.rb', __FILE__)
# require File.expand_path('../pre-env-initializers/load_email_config.rb', __FILE__)
