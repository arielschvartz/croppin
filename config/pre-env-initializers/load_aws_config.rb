require 'aws-sdk'
require 'paperclip'

raw_config = File.read("#{Rails.root}/config/aws_config.yml")
AWS_CONFIG = YAML.load(raw_config)[Rails.env].symbolize_keys

AWS_CONFIG[:access_key_id] = ENV['AWS_ACCESS_KEY_ID']
AWS_CONFIG[:secret_access_key] = ENV['AWS_SECRET_ACCESS_KEY']

Rails.application.configure do
  config.paperclip_defaults = {
    storage: :s3,
    url: 's3_domain_url',
    path: 'uploads/:class/:id/:attachment/:style.:extension',
    storage: :s3,
    s3_credentials: AWS_CONFIG,
    s3_permissions: :private,
    s3_protocol: 'https',
    s3_host_name: AWS_CONFIG[:region]
  }
end
