class Business < ActiveRecord::Base
  acts_as_paranoid

  include WithEmail
  include WithCnpj

  extend FriendlyId
  friendly_id :name

  has_attached_file :logo, :styles => { :small => "85x85#", :medium => "300x300#", :thumb => "110x138#" }
  validates_attachment :logo, presence: true, content_type: { content_type: /\Aimage/ }, file_name: { matches: [/png\Z/, /gif\Z/, /jpe?g\Z/, /JPE?G\Z/] }

  validates_presence_of :name, :about

  validates_uniqueness_of :name

  has_many :branches
  has_one :address, as: :owner, dependent: :destroy
  has_many :phones, as: :owner, dependent: :destroy

  after_save :update_branches_slugs, if: :should_generate_new_friendly_id?

  private

    def should_generate_new_friendly_id?
      self.slug.blank? || self.name_changed?
    end

    def update_branches_slugs
      self.branches.each do |b|
        b.save
      end
    end

end
