class User < ActiveRecord::Base
  acts_as_paranoid

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable
  devise :database_authenticatable, :registerable, :omniauthable, :recoverable, :rememberable, :trackable, :validatable

  has_many :promotion_views
  has_many :promotion_clicks
  has_many :promotion_likes
  has_many :promotion_reports

  has_and_belongs_to_many :aggregators
end
