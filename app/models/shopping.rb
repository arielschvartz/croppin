class Shopping < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :administration_business, class_name: "Business"

  extend FriendlyId
  friendly_id :name

  has_attached_file :logo, :styles => { :medium => "300x300", :small => "100x100", :thumb => "35x35" }, :convert_options => { :medium => "-quality 100 -strip -gravity Center", :small => "-quality 80 -strip -gravity Center", :thumb => "-quality 75 -strip -gravity Center" }
  validates_attachment :logo, presence: true, content_type: { content_type: /\Aimage/ }, file_name: { matches: [/png\Z/, /gif\Z/, /jpe?g\Z/, /JPE?G\Z/] }

  validates_presence_of :name, :address, :phones
  validates_uniqueness_of :name

  has_many :branches
  has_one :address, as: :owner, dependent: :destroy
  has_many :phones, as: :owner, dependent: :destroy

  private

    def should_generate_new_friendly_id?
      self.slug.blank? || self.name_changed?
    end
end
