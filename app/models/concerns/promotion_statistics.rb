module PromotionStatistics
  extend ActiveSupport::Concern

  included do
    acts_as_paranoid

    belongs_to :promotion
    belongs_to :user

    validates_presence_of :promotion, :ip, :user

    validates :ip, presence: true, format: { with: Resolv::IPv4::Regex }

    after_validation :set_geocode

    after_save :increment

    def set_geocode_info geo_hash
      if geo_hash[:latitude].present? and geo_hash[:longitude].present? and geo_hash[:country].present?
        self.latitude = geo_hash[:latitude]
        self.longitude = geo_hash[:longitude]
        self.country = geo_hash[:country]
        self.state = geo_hash[:state]
        self.city = geo_hash[:city]
      end
    end
  end

  def increment
    self.promotion.increment("number_of_#{self.class.to_s[9..-1].downcase}s".to_sym)

    if self.promotion.category.present?
      self.promotion.category.increment("number_of_#{self.class.to_s[9..-1].downcase}s".to_sym)
    end
  end

  def set_geocode
    unless self.latitude.present? or self.longitude.present? or self.country.present? or self.state.present? or self.city.present?
      geo_result = Geocoder.search(self.ip).first
      self.latitude = geo_result.latitude
      self.longitude = geo_result.longitude
      self.country = geo_result.country
      self.state = geo_result.state
      self.city = geo_result.city
    end
  end
end
