module WithCnpj
  extend ActiveSupport::Concern

  included do
    validates_uniqueness_of :cnpj, allow_nil: true, allow_blank: false
    validates :cnpj, cnpj: true

    before_save :strip_cnpj

    def formatted_cnpj
      if self.cnpj.present?
        CNPJ.new(self.cnpj).formatted
      else
        nil
      end
    end
  end

  def strip_cnpj
    unless self.cnpj.blank?
      self.cnpj = CNPJ.new(self.cnpj).stripped
    end
  end
end