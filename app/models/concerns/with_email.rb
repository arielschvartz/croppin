module WithEmail
  extend ActiveSupport::Concern

  included do
    validates :email, email: true

    before_save :downcase_email
  end

  def downcase_email
    self.email.downcase! unless self.email.blank?
  end
end