class Branch < ActiveRecord::Base
  acts_as_paranoid

  extend FriendlyId
  friendly_id :custom_slug

  belongs_to :business
  belongs_to :shopping

  has_one :address, as: :owner, dependent: :destroy, autosave: true
  has_many :phones, as: :owner, dependent: :destroy, autosave: true

  has_and_belongs_to_many :promotions

  validates_presence_of :business
  validates_presence_of :address, if: -> { website.blank? }
  validates_presence_of :website, if: -> { address.nil? }

  def phone
    self.phones.where(default: true).first || self.phones.first
  end

  def should_generate_new_friendly_id?
    self.slug.blank? || self.method(:custom_slug).call != self.slug
  end

  def name
    "#{self.business.present? ? self.business.slug.split('-').map(&:capitalize).join(' ') : ''} - #{self.address.present? ? self.address.neighborhood : 'online'}"
  end

  private

    def custom_slug
      "#{self.business.present? ? self.business.slug : ''}-#{self.address.present? ? self.address.neighborhood : 'online'}".downcase
    end

end
