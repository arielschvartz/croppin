class Category < ActiveRecord::Base
  acts_as_paranoid

  extend FriendlyId
  friendly_id :name

  acts_as_nested_set

  has_attached_file :icon, :styles => { :medium => "300x300>", :thumb => "100x100>" }

  has_many :promotions
  has_many :highlights, -> { where(highlight: true) }, class_name: 'Promotion', foreign_key: 'category_id'

  has_many :promotion_views, through: :promotions
  has_many :promotion_clicks, through: :promotions
  has_many :promotion_likes, through: :promotions
  has_many :promotion_reports, through: :promotions

  validates_presence_of :name
  validates_uniqueness_of :name

  def color
    if self.attributes['color'].present?
      self.read_attribute(:color)
    elsif self.parent.present?
      self.parent.color
    else
      '#000'
    end
  end

  private

    def should_generate_new_friendly_id?
      self.slug.blank? || self.name_changed?
    end
end
