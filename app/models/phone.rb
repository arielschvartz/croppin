class Phone < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :owner, polymorphic: true

  validates_presence_of :country_code, :number
  before_validation :set_country_code, if: -> { self.country_code.blank? }

  def full_number no_country_code = false
    "#{self.country_code.to_s.rjust(2, "0") unless no_country_code}#{self.city_code.to_s.rjust(2, "0")}#{self.number}"
  end

  def formatted local = nil, international = true
    if self.city_code.blank?
      return self.number
    end

    c = Country.find_by_alpha2(local)

    if c.nil?
      c = Country.find_by_country_code(self.country_code)
    end

    if c
      local = c[0].to_sym if local.nil?
      c = c[1]
      if international
        full_number(true).phony_formatted(normalize: local.to_sym, format: :international)
      else
        full_number(true).phony_formatted(normalize: local.to_sym)
      end
    else
      full_number.phony_formatted(format: :international)
      # raise ArgumentError, "country not found"
    end
  end

  def set_from_full_number new_number
    l = new_number.length
    if [12, 13].include? l
      self.country_code = new_number[0..1].to_i
      self.city_code = new_number[2..3].to_i
      self.number = new_number[4..-1]
    elsif [10, 11].include? l
      self.city_code = new_number[0..1].to_i
      self.number = new_number[2..-1]
    elsif l < 10
      self.city_code = 00
      self.number = new_number
    end
  end

  private

    def set_country_code
      self.country_code = 55
    end
end
