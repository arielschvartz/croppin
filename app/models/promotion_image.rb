class PromotionImage < ActiveRecord::Base
  acts_as_paranoid

  default_scope { where(deleted_at: nil).order(:promotion_id, :position) }

  belongs_to :promotion

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment :image, presence: true, content_type: { content_type: /\Aimage/ }, file_name: { matches: [/png\Z/, /jpe?g\Z/, /JPE?G\Z/] }

  # validates_presence_of :promotion
end
