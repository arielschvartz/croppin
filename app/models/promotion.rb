class Promotion < ActiveRecord::Base
  acts_as_paranoid

  extend FriendlyId
  friendly_id :title

  enum difficulty: { easy: 0, medium: 1, hard: 2, extreme: 3, impossible: 4 }

  def translated_difficulty
    case self.difficulty
    when 'easy'
      'Fácil'
    when 'medium'
      'Médio'
    when 'hard'
      'Difícil'
    when 'extreme'
      'Extrema'
    when 'impossible'
      'Impossível'
    end
  end

  belongs_to :aggregator
  belongs_to :category

  has_and_belongs_to_many :branches

  has_many :promotion_images, dependent: :destroy, autosave: true
  has_one :open_day, dependent: :destroy, autosave: true

  has_many :promotion_views
  has_many :promotion_clicks
  has_many :promotion_likes
  has_many :promotion_reports

  has_many :businesses, -> { uniq }, through: :branches

  validates_presence_of :title, :description, :website

  validates_numericality_of :points_needed, only_integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :difficulty, only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 5

  validates_format_of :website, with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/

  def business
    self.businesses.first
  end

  def branch
    self.branches.first
  end

  private

    def should_generate_new_friendly_id?
      self.slug.blank? || self.title_changed?
    end
end
