class Address < ActiveRecord::Base
  acts_as_paranoid

  include WithZipCode

  belongs_to :owner, polymorphic: true

  validates_presence_of :country, :latitude, :longitude

  before_validation :set_country

  after_save :update_owner_slug, if: :should_generate_new_friendly_id?
  after_destroy :update_owner_slug

  before_validation :set_info, if: -> { (self.latitude.present? and self.longitude.present?) and (self.zip_code.blank? or self.street.blank? or self.country.blank? or self.state.blank? or self.city.blank?) }
  before_validation :set_latlong, if: -> { (self.latitude.blank? and self.longitude.blank?) and (self.street.present? and self.number.present? and self.city.present?) }

  def country_name
    Country.new(self.country).name
  end

  def full_street
    value = ''
    if self.street.present?
      value += self.street
      if self.number.present?
        value += ", #{self.number}"
        if self.complement
          value += " / #{self.complement}"
        end
      end
    end
    value
  end

  def full_city
    value = ''
    value += self.city if self.city.present?
    value += ', ' if self.state.present? and not value.empty?
    value += self.state if self.state.present?
    value += ' - ' if self.zip_code.present? and not self.zip_code.empty?
    value += self.formatted_zip_code if self.zip_code.present?
  end

  private

    def set_country
      if self.country.blank?
        self.country = "BR"
      end
    end

    def should_generate_new_friendly_id?
      if self.changed? and self.owner.present? and self.owner.respond_to? :should_generate_new_friendly_id?
        self.owner.should_generate_new_friendly_id?
      else
        false
      end
    end

    def update_owner_slug
      self.owner.save if self.owner.present?
    end

    def set_info
      unless self.zip_code.present? and self.street.present? and self.country.present? and self.state.present? and self.city.present?
        geo_result = Geocoder.search("#{self.latitude},#{self.longitude}").first
        self.country = Country.find_by_name(geo_result.country).last['alpha2']
        self.state ||= geo_result.state_code
        self.city ||= geo_result.city
        self.neighborhood ||= geo_result.neighborhood
        self.street ||= geo_result.route
        self.number ||= geo_result.street_number
        if geo_result.postal_code
          self.zip_code ||= BrZipCode.strip(geo_result.postal_code)
        end
      end
    end

    def set_latlong
      geo_result = Geocoder.search("#{self.street} #{self.number}, #{self.city}, #{self.state}").first
      self.latitude = geo_result.latitude
      self.longitude = geo_result.longitude
    end
end
