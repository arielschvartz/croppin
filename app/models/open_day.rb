class OpenDay < ActiveRecord::Base
  self.primary_key = :promotion_id

  acts_as_paranoid

  belongs_to :promotion
  validates_presence_of :promotion

  def set_days args
    if args.class.to_s.downcase == "string"
      values = args.scan(/[0,1]/)
      if args.length == values.length and values.length == 7
        values.each_with_index do |v, i|
          self[days[i]] = (v == "0" ? false : true)
        end
      else
        raise ArgumentError, "wrong number of binaries"
      end
    else
      raise ArgumentError, "must be a string"
    end
  end

  def humanize
    if only_weekdays?
      "somente dias de semana"
    elsif only_weekend?
      "somente finais de semana"
    else
      open_days = []
      days.each_with_index do |d, i|
        if self[d]
          open_days.push(I18n.t(:'date.day_names')[i].downcase)
        end
      end
      if open_days.empty?
        "fechado"
      else
        open_days.join(", ")
      end
    end
  end

  def open_at? day
    if days.include? day
      self[day]
    else
      raise ArgumentError, "must be a valid day"
    end
  end

  def days
    [:sun, :mon, :tue, :wed, :thu, :fri, :sat]
  end

  def days_letters
    { sun: "D", mon: "S", tue: "T", wed: "Q", thu: "Q", fri: "S", sat: "S" }
  end

  private
    def only_weekdays?
      days.each do |d|
        if weekdays.include? d
          return false unless self[d]
        else
          return false if self[d]
        end
      end
      return true
    end

    def only_weekend?
      days.each do |d|
        if weekdays.include? d
          return false if self[d]
        else
          return false unless self[d]
        end
      end
      return true
    end

    def weekdays
      [:mon, :tue, :wed, :thu, :fri]
    end
end
