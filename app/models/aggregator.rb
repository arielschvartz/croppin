class Aggregator < ActiveRecord::Base
  acts_as_paranoid

  extend FriendlyId
  friendly_id :name

  has_attached_file :logo, :styles => { :small => "42x42" }, :convert_options => { :small => "-quality 100 -strip -gravity Center" }
  validates_attachment :logo, presence: true, content_type: { content_type: /\Aimage/ }, file_name: { matches: [/png\Z/, /jpe?g\Z/] }

  has_attached_file :logo_white, :styles => { :small => "42x42" }, :convert_options => { :small => "-quality 100 -strip -gravity Center" }
  validates_attachment :logo_white, presence: true, content_type: { content_type: /\Aimage/ }, file_name: { matches: [/png\Z/, /jpe?g\Z/] }

  validates_format_of :website, with: /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/

  validates_presence_of :name, :website
  validates_uniqueness_of :name

  has_many :promotions
  has_and_belongs_to_many :users

  private

    def should_generate_new_friendly_id?
      self.slug.blank? || self.name_changed?
    end

end
