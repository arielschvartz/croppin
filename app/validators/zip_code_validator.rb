class ZipCodeValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless value.blank?
      unless value.scan(/[0-9]/).length == 8
        record.errors[attribute] << (options[:message] || "is not a valid zip code")
      end
    end
  end

end