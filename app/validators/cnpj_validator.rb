class CnpjValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless value.blank?
      unless CNPJ.valid?(value)
        record.errors[attribute] << (options[:message] || "is not a valid CNPJ")
      end
    end
  end

end