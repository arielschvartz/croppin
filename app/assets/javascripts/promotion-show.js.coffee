TILE_SIZE = 256
center = undefined
map = undefined
markers = []

bound = (value, opt_min, opt_max) ->
  value = Math.max(value, opt_min)  if opt_min?
  value = Math.min(value, opt_max)  if opt_max?
  value

degreesToRadians = (deg) ->
  deg * (Math.PI / 180)

radiansToDegrees = (rad) ->
  rad / (Math.PI / 180)

###
@constructor
###

MercatorProjection = ->
  @pixelOrigin_ = new google.maps.Point(TILE_SIZE / 2, TILE_SIZE / 2)
  @pixelsPerLonDegree_ = TILE_SIZE / 360
  @pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI)
  return

# Truncating to 0.9999 effectively limits latitude to 89.189. This is
# about a third of a tile past the edge of the world tile.
createInfoWindowContent = ->
  numTiles = 1 << map.getZoom()
  projection = new MercatorProjection()
  worldCoordinate = projection.fromLatLngToPoint(center)
  pixelCoordinate = new google.maps.Point(worldCoordinate.x * numTiles, worldCoordinate.y * numTiles)
  tileCoordinate = new google.maps.Point(Math.floor(pixelCoordinate.x / TILE_SIZE), Math.floor(pixelCoordinate.y / TILE_SIZE))
  [
    "Chicago, IL"
    "LatLng: " + center.lat() + " , " + center.lng()
    "World Coordinate: " + worldCoordinate.x + " , " + worldCoordinate.y
    "Pixel Coordinate: " + Math.floor(pixelCoordinate.x) + " , " + Math.floor(pixelCoordinate.y)
    "Tile Coordinate: " + tileCoordinate.x + " , " + tileCoordinate.y + " at Zoom Level: " + map.getZoom()
  ].join "<br>"

initialize = ->
  mapOptions =
    zoom: 15
    center: center
    scrollwheel: false

  map = new google.maps.Map(document.getElementById("promotion-business-map"), mapOptions)
  coordInfoWindow = new google.maps.InfoWindow()
  # coordInfoWindow.setContent createInfoWindowContent()
  coordInfoWindow.setPosition center
  coordInfoWindow.open map
  google.maps.event.addListener map, "zoom_changed", ->
    # coordInfoWindow.setContent createInfoWindowContent()
    coordInfoWindow.open map
    return

  return

MercatorProjection::fromLatLngToPoint = (latLng, opt_point) ->
  me = this
  point = opt_point or new google.maps.Point(0, 0)
  origin = me.pixelOrigin_
  point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_
  siny = bound(Math.sin(degreesToRadians(latLng.lat())), -0.9999, 0.9999)
  point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -me.pixelsPerLonRadian_
  point

MercatorProjection::fromPointToLatLng = (point) ->
  me = this
  origin = me.pixelOrigin_
  lng = (point.x - origin.x) / me.pixelsPerLonDegree_
  latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_
  lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2)
  new google.maps.LatLng(lat, lng)

$ ->
  if $('#promotion-business-map').length > 0
    names = []
    $('#map-markers').find('.marker').each ->
      markers.push(new google.maps.LatLng($(this).data('latitude'), $(this).data('longitude')))
      if $(this).data('current')
        center = markers[markers.length - 1]
      names.push $(this).data('name')

    initialize()

    counter = 0
    for i in markers
      new google.maps.Marker
        position: i,
        map: map,
        title: names[counter]
      counter++
