class CategoriesController < InheritedResources::Base
  actions :show

  def show
    show! do |format|
      @promotions = @category.promotions
      @title = @category.name
      format.html { render 'promotions/index' }
    end
  end
end
