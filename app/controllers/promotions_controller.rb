class PromotionsController < InheritedResources::Base
  actions :index, :show

  def show
    show! do
      @category = @promotion.category
      @title = @category.name
      @branch = @promotion.branch
      @business = @branch.business
      @address = @branch.address
      @phone = @branch.phone
    end
  end

  protected

    def collection
      @promotions ||= end_of_association_chain.limit(20)
    end
end
