# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140822020024) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "zip_code",     limit: 8
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state",        limit: 2
    t.string   "country",                default: "BRA"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "default",                default: false
  end

  add_index "addresses", ["deleted_at"], name: "index_addresses_on_deleted_at", using: :btree
  add_index "addresses", ["owner_id", "owner_type"], name: "index_addresses_on_owner_id_and_owner_type", using: :btree
  add_index "addresses", ["state", "city", "neighborhood"], name: "index_addresses_on_state_and_city_and_neighborhood", using: :btree
  add_index "addresses", ["zip_code"], name: "index_addresses_on_zip_code", using: :btree

  create_table "aggregators", force: true do |t|
    t.string   "name"
    t.string   "website"
    t.text     "participants_details"
    t.integer  "default_difficulty"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "logo_white_file_name"
    t.string   "logo_white_content_type"
    t.integer  "logo_white_file_size"
    t.datetime "logo_white_updated_at"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "aggregators", ["deleted_at"], name: "index_aggregators_on_deleted_at", using: :btree

  create_table "aggregators_users", id: false, force: true do |t|
    t.integer  "aggregator_id"
    t.integer  "user_id"
    t.datetime "deleted_at"
  end

  add_index "aggregators_users", ["aggregator_id"], name: "index_aggregators_users_on_aggregator_id", using: :btree
  add_index "aggregators_users", ["deleted_at"], name: "index_aggregators_users_on_deleted_at", using: :btree
  add_index "aggregators_users", ["user_id", "aggregator_id"], name: "index_aggregators_users_on_user_id_and_aggregator_id", using: :btree

  create_table "branches", force: true do |t|
    t.integer  "business_id"
    t.integer  "shopping_id"
    t.string   "slug"
    t.string   "website"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "branches", ["business_id"], name: "index_branches_on_business_id", using: :btree
  add_index "branches", ["deleted_at"], name: "index_branches_on_deleted_at", using: :btree
  add_index "branches", ["shopping_id"], name: "index_branches_on_shopping_id", using: :btree
  add_index "branches", ["slug"], name: "index_branches_on_slug", unique: true, using: :btree

  create_table "branches_promotions", id: false, force: true do |t|
    t.integer  "branch_id"
  t.integer  "promotion_id"
    t.datetime "deleted_at"
  end

  add_index "branches_promotions", ["branch_id", "promotion_id"], name: "index_branches_promotions_on_branch_id_and_promotion_id", using: :btree
  add_index "branches_promotions", ["deleted_at"], name: "index_branches_promotions_on_deleted_at", using: :btree
  add_index "branches_promotions", ["promotion_id"], name: "index_branches_promotions_on_promotion_id", using: :btree

  create_table "businesses", force: true do |t|
    t.string   "corporate_name"
    t.string   "name"
    t.string   "email"
    t.string   "slug"
    t.string   "cnpj"
    t.text     "about"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "default_difficulty"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "businesses", ["deleted_at"], name: "index_businesses_on_deleted_at", using: :btree
  add_index "businesses", ["name"], name: "index_businesses_on_name", using: :btree
  add_index "businesses", ["slug"], name: "index_businesses_on_slug", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.integer  "number_of_reports", default: 0
    t.integer  "number_of_likes",   default: 0
    t.integer  "number_of_clicks",  default: 0
    t.integer  "number_of_views",   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "color"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.string   "icon_code"
  end

  add_index "categories", ["deleted_at"], name: "index_categories_on_deleted_at", using: :btree
  add_index "categories", ["depth"], name: "index_categories_on_depth", using: :btree
  add_index "categories", ["lft"], name: "index_categories_on_lft", using: :btree
  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree
  add_index "categories", ["rgt"], name: "index_categories_on_rgt", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "open_days", id: false, force: true do |t|
    t.integer  "promotion_id",                 null: false
    t.boolean  "sun",          default: false
    t.boolean  "mon",          default: false
    t.boolean  "tue",          default: false
    t.boolean  "wed",          default: false
    t.boolean  "thu",          default: false
    t.boolean  "fri",          default: false
    t.boolean  "sat",          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "open_days", ["deleted_at"], name: "index_open_days_on_deleted_at", using: :btree
  add_index "open_days", ["promotion_id"], name: "index_open_days_on_promotion_id", using: :btree

  create_table "phones", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "country_code"
    t.integer  "city_code"
    t.string   "number",         limit: 12
    t.integer  "extension_line"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "default",                   default: false
  end

  add_index "phones", ["country_code", "city_code", "number"], name: "index_phones_on_country_code_and_city_code_and_number", using: :btree
  add_index "phones", ["deleted_at"], name: "index_phones_on_deleted_at", using: :btree
  add_index "phones", ["owner_id", "owner_type"], name: "index_phones_on_owner_id_and_owner_type", using: :btree

  create_table "promotion_clicks", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "user_id"
    t.string   "ip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotion_clicks", ["country", "state", "city"], name: "index_promotion_clicks_on_country_and_state_and_city", using: :btree
  add_index "promotion_clicks", ["deleted_at"], name: "index_promotion_clicks_on_deleted_at", using: :btree
  add_index "promotion_clicks", ["promotion_id"], name: "index_promotion_clicks_on_promotion_id", using: :btree
  add_index "promotion_clicks", ["user_id"], name: "index_promotion_clicks_on_user_id", using: :btree

  create_table "promotion_images", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "position"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotion_images", ["deleted_at"], name: "index_promotion_images_on_deleted_at", using: :btree
  add_index "promotion_images", ["promotion_id", "position"], name: "index_promotion_images_on_promotion_id_and_position", using: :btree
  add_index "promotion_images", ["promotion_id"], name: "index_promotion_images_on_promotion_id", using: :btree

  create_table "promotion_likes", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "user_id"
    t.string   "ip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotion_likes", ["country", "state", "city"], name: "index_promotion_likes_on_country_and_state_and_city", using: :btree
  add_index "promotion_likes", ["deleted_at"], name: "index_promotion_likes_on_deleted_at", using: :btree
  add_index "promotion_likes", ["promotion_id"], name: "index_promotion_likes_on_promotion_id", using: :btree
  add_index "promotion_likes", ["user_id"], name: "index_promotion_likes_on_user_id", using: :btree

  create_table "promotion_reports", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "user_id"
    t.string   "ip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotion_reports", ["country", "state", "city"], name: "index_promotion_reports_on_country_and_state_and_city", using: :btree
  add_index "promotion_reports", ["deleted_at"], name: "index_promotion_reports_on_deleted_at", using: :btree
  add_index "promotion_reports", ["promotion_id"], name: "index_promotion_reports_on_promotion_id", using: :btree
  add_index "promotion_reports", ["user_id"], name: "index_promotion_reports_on_user_id", using: :btree

  create_table "promotion_views", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "user_id"
    t.string   "ip"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotion_views", ["country", "state", "city"], name: "index_promotion_views_on_country_and_state_and_city", using: :btree
  add_index "promotion_views", ["deleted_at"], name: "index_promotion_views_on_deleted_at", using: :btree
  add_index "promotion_views", ["promotion_id"], name: "index_promotion_views_on_promotion_id", using: :btree
  add_index "promotion_views", ["user_id"], name: "index_promotion_views_on_user_id", using: :btree

  create_table "promotions", force: true do |t|
    t.integer  "aggregator_id"
    t.integer  "category_id"
    t.string   "title"
    t.text     "description"
    t.text     "restriction"
    t.text     "terms_of_use"
    t.string   "slug"
    t.text     "website"
    t.integer  "points_needed"
    t.integer  "difficulty"
    t.date     "expiration_date"
    t.integer  "number_of_reports", default: 0
    t.integer  "number_of_likes",   default: 0
    t.integer  "number_of_clicks",  default: 0
    t.integer  "number_of_views",   default: 0
    t.boolean  "highlight",         default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "promotions", ["aggregator_id", "category_id", "highlight"], name: "index_promotions_on_aggregator_id_and_category_id_and_highlight", using: :btree
  add_index "promotions", ["aggregator_id"], name: "index_promotions_on_aggregator_id", using: :btree
  add_index "promotions", ["category_id"], name: "index_promotions_on_category_id", using: :btree
  add_index "promotions", ["deleted_at"], name: "index_promotions_on_deleted_at", using: :btree

  create_table "shoppings", force: true do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "website"
    t.integer  "administration_business_id"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "shoppings", ["administration_business_id"], name: "index_shoppings_on_administration_business_id", using: :btree
  add_index "shoppings", ["deleted_at"], name: "index_shoppings_on_deleted_at", using: :btree
  add_index "shoppings", ["name"], name: "index_shoppings_on_name", using: :btree
  add_index "shoppings", ["slug"], name: "index_shoppings_on_slug", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
