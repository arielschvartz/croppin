class CreatePromotionImages < ActiveRecord::Migration
  def change
    create_table :promotion_images do |t|
      t.references :promotion, index: true
      t.integer :position

      t.attachment :image

      t.timestamps
    end

    add_index :promotion_images, [:promotion_id, :position]

    add_column :promotion_images, :deleted_at, :datetime
    add_index :promotion_images, :deleted_at
  end
end
