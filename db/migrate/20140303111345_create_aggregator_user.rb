class CreateAggregatorUser < ActiveRecord::Migration
  def change
    create_table :aggregators_users, id: false do |t|
      t.belongs_to :aggregator
      t.belongs_to :user
    end

    add_index :aggregators_users, [:user_id, :aggregator_id]
    add_index :aggregators_users, [:aggregator_id]

    add_column :aggregators_users, :deleted_at, :datetime
    add_index :aggregators_users, :deleted_at
  end
end
