class CreateAggregators < ActiveRecord::Migration
  def change
    create_table :aggregators do |t|
      t.string :name
      t.string :website
      t.text :participants_details
      t.integer :default_difficulty
      t.attachment :logo
      t.attachment :logo_white
      t.string :slug

      t.timestamps
    end

    add_column :aggregators, :deleted_at, :datetime
    add_index :aggregators, :deleted_at
  end
end
