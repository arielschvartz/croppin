class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth

      t.integer :number_of_reports, default: 0
      t.integer :number_of_likes, default: 0
      t.integer :number_of_clicks, default: 0
      t.integer :number_of_views, default: 0

      t.timestamps
    end

    add_index :categories, :parent_id
    add_index :categories, :lft
    add_index :categories, :rgt
    add_index :categories, :depth

    add_column :categories, :deleted_at, :datetime
    add_index :categories, :deleted_at
  end
end
