class CreatePromotionLikes < ActiveRecord::Migration
  def change
    create_table :promotion_likes do |t|
      t.references :promotion, index: true
      t.references :user, index: true
      t.string :ip
      t.float :latitude
      t.float :longitude
      t.string :country
      t.string :state
      t.string :city

      t.timestamps
    end

    add_index :promotion_likes, [:country, :state, :city]

    add_column :promotion_likes, :deleted_at, :datetime
    add_index :promotion_likes, :deleted_at
  end
end
