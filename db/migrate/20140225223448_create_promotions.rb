class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.references :aggregator, index: true
      t.references :category, index: true
      t.string :title
      t.text :description
      t.text :restriction
      t.text :terms_of_use
      t.string :slug
      t.string :website
      t.integer :points_needed
      t.integer :difficulty
      t.date :expiration_date
      t.integer :number_of_reports, default: 0
      t.integer :number_of_likes, default: 0
      t.integer :number_of_clicks, default: 0
      t.integer :number_of_views, default: 0

      t.boolean :highlight, default: false

      t.timestamps
    end

    add_index :promotions, [:aggregator_id, :category_id, :highlight]

    add_column :promotions, :deleted_at, :datetime
    add_index :promotions, :deleted_at
  end
end
