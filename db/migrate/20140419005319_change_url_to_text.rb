class ChangeUrlToText < ActiveRecord::Migration
  def change
    change_column :promotions, :website, :text
  end
end
