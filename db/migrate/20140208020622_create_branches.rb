class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.belongs_to :business, index: true
      t.belongs_to :shopping, index: true

      t.string :slug

      t.string :website

      t.timestamps
    end

    add_index :branches, :slug, unique: true

    add_column :branches, :deleted_at, :datetime
    add_index :branches, :deleted_at
  end
end
