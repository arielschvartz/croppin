class CreateBranchPromotion < ActiveRecord::Migration
  def change
    create_table :branches_promotions, id: false do |t|
      t.belongs_to :branch
      t.belongs_to :promotion
    end

    add_index :branches_promotions, [:branch_id, :promotion_id]
    add_index :branches_promotions, :promotion_id

    add_column :branches_promotions, :deleted_at, :datetime
    add_index :branches_promotions, :deleted_at
  end
end
