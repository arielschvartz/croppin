class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :corporate_name
      t.string :name
      t.string :email
      t.string :slug

      t.string :cnpj
      t.text :about

      t.attachment :logo

      t.integer :default_difficulty

      t.timestamps
    end

    add_index :businesses, :slug, unique: true
    add_index :businesses, :name

    add_column :businesses, :deleted_at, :datetime
    add_index :businesses, :deleted_at
  end
end
