class CreatePromotionViews < ActiveRecord::Migration
  def change
    create_table :promotion_views do |t|
      t.references :promotion, index: true
      t.references :user, index: true
      t.string :ip
      t.float :latitude
      t.float :longitude
      t.string :country
      t.string :state
      t.string :city

      t.timestamps
    end

    add_index :promotion_views, [:country, :state, :city]

    add_column :promotion_views, :deleted_at, :datetime
    add_index :promotion_views, :deleted_at
  end
end
