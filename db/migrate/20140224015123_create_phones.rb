class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.references :owner, index: true, polymorphic: true
      t.integer :country_code
      t.integer :city_code
      t.string :number, limit: 12
      t.integer :extension_line

      t.timestamps
    end

    add_index :phones, [:country_code, :city_code, :number]

    add_column :phones, :deleted_at, :datetime
    add_index :phones, :deleted_at
  end
end
