class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :owner, index: true, polymorphic: true
      t.string :zip_code, limit: 8
      t.string :street
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :city
      t.string :state, limit: 2
      t.string :country, default: "BRA"
      t.float :latitude
      t.float :longitude

      t.timestamps
    end

    add_index :addresses, :zip_code
    add_index :addresses, [:state, :city, :neighborhood]

    add_column :addresses, :deleted_at, :datetime
    add_index :addresses, :deleted_at
  end
end
