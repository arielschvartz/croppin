class AddInfoToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :color, :string
    add_attachment :categories, :icon
  end
end
