class CreateShoppings < ActiveRecord::Migration
  def change
    create_table :shoppings do |t|
      t.string :name
      t.string :slug
      t.string :website
      t.references :administration_business, index: true

      t.attachment :logo

      t.timestamps
    end

    add_index :shoppings, :slug, unique: true
    add_index :shoppings, :name

    add_column :shoppings, :deleted_at, :datetime
    add_index :shoppings, :deleted_at
  end
end
