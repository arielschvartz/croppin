class AddIconCodeToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :icon_code, :string
  end
end
