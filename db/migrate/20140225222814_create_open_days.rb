class CreateOpenDays < ActiveRecord::Migration
  def change
    create_table :open_days, id: false, primary_key: 'promotion_id' do |t|
      t.references :promotion, index: true, null: false
      t.boolean :sun, default: false
      t.boolean :mon, default: false
      t.boolean :tue, default: false
      t.boolean :wed, default: false
      t.boolean :thu, default: false
      t.boolean :fri, default: false
      t.boolean :sat, default: false

      t.timestamps
    end

    add_column :open_days, :deleted_at, :datetime
    add_index :open_days, :deleted_at
  end
end
