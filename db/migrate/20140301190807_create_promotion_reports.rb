class CreatePromotionReports < ActiveRecord::Migration
  def change
    create_table :promotion_reports do |t|
      t.references :promotion, index: true
      t.references :user, index: true
      t.string :ip
      t.float :latitude
      t.float :longitude
      t.string :country
      t.string :state
      t.string :city

      t.timestamps
    end

    add_index :promotion_reports, [:country, :state, :city]

    add_column :promotion_reports, :deleted_at, :datetime
    add_index :promotion_reports, :deleted_at
  end
end
