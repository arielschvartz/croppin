class AddDefaultToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :default, :boolean, default: false
  end
end
